package nl.robertvankammen.scoreboard;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.Statistic;

import java.util.HashMap;

public class ScoreboardInfo {
    private final String displayname;
    private final Statistic statistic;
    private int calculateFactor;
    private HashMap<String, ScoreboardEntityTypeInfo> subEntitys;

    public ScoreboardInfo(String displayname, Statistic statistic) {
        this.displayname = displayname;
        this.statistic = statistic;
    }

    public ScoreboardInfo(String displayname, Statistic statistic, int calculateFactor) {
        this(displayname, statistic);
        this.calculateFactor = calculateFactor;
    }

    public ScoreboardInfo(String displayname, Statistic statistic, HashMap<String, ScoreboardEntityTypeInfo> subEntitys) {
        this(displayname, statistic);
        this.subEntitys = subEntitys;
    }

    public String getDisplayname() {
        return StringUtils.capitalize(displayname.replace("_", " "));
    }

    public Statistic getStatistic() {
        return statistic;
    }

    public int getCalculateFactor() {
        return calculateFactor;
    }

    public HashMap<String, ScoreboardEntityTypeInfo> getSubEntitys() {
        if (subEntitys == null) {
            subEntitys = new HashMap<>();
        }
        return subEntitys;
    }
}
