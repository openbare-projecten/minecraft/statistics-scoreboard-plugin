package nl.robertvankammen.scoreboard;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;

import java.util.List;

public class ScoreboardEntityTypeInfo {
    private EntityType entityType;
    private Material material;
    private List<Material> materials;
    private boolean merge = false;
    private final String displayname;

    public ScoreboardEntityTypeInfo(String displayname, EntityType entityType) {
        this.displayname = displayname;
        this.entityType = entityType;
    }

    public ScoreboardEntityTypeInfo(String displayname, Material material) {
        this.displayname = displayname;
        this.material = material;
    }

    public ScoreboardEntityTypeInfo(String displayname, boolean merge, List<Material> materials) {
        this.displayname = displayname;
        this.merge = merge;
        this.materials = materials;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public Material getMaterial() {
        return material;
    }

    public String getDisplayname() {
        return displayname.replace("_", " ");
    }

    public List<Material> getMaterials() {
        return materials;
    }

    public boolean isMerge() {
        return merge;
    }
}
