package nl.robertvankammen.scoreboard;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.Style;
import net.kyori.adventure.text.format.TextColor;
import net.kyori.adventure.text.format.TextDecoration;
import org.apache.commons.lang3.ClassUtils;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Mob;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Criteria;
import org.bukkit.scoreboard.Objective;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static java.util.Collections.reverseOrder;
import static java.util.stream.Collectors.toList;
import static org.bukkit.Material.*;
import static org.bukkit.Statistic.*;
import static org.bukkit.Statistic.Type.UNTYPED;
import static org.bukkit.entity.EntityType.UNKNOWN;
import static org.bukkit.scoreboard.DisplaySlot.SIDEBAR;

public class SimpleScoreboard extends JavaPlugin implements Listener {

    private static final TreeMap<String, ScoreboardInfo> SCOREBOARD_INFO_MAP = new TreeMap<>();
    private static final HashMap<Player, String[]> playerSubCommand = new HashMap<>();
    private static final Set<String> IGNORE_PLAYER_NAMES = new HashSet<>();
    private static final String COMMAND_STRING = "scb";
    private static final String COMMAND_SUB_CLEAR = "clear";
    private static final Integer TICKS_TO_MINUTES = 20 * 60;
    private static final Integer TICKS_TO_HOURS = TICKS_TO_MINUTES * 60;
    private static final Integer CM_TO_M = 100;
    private static final Integer CM_TO_KM = CM_TO_M * 1000;
    private static final String CONFIG_PATH_IGNORE = "ignore.players";
    private static final String CONFIG_PATH_AMOUNT_OF_PLAYERS = "amountPlayers";
    private static final String CONFIG_PATH_REFRESH_TIME = "refreshtime";

    private static Integer playerAmount = 10;
    private static Integer refreshTime = 100;

    private int taskId;

    @Override
    public void onEnable() {
        this.getServer().getPluginManager().registerEvents(this, this);
        checkConfig();
        addStatistic();
        addKilled();
        addMine();
        addUse();
//        SCOREBOARD_INFO_MAP.forEach((s, scoreboardInfo) -> {
//            if (scoreboardInfo != null) {
//                if (!scoreboardInfo.getSubEntitys().isEmpty()) {
//                    scoreboardInfo.getSubEntitys().forEach((s1, scoreboardEntityTypeInfo) -> {
//                        //System.out.printf("/scb %s %s%n", s, s1);
//                    });
//                } else {
//                    System.out.printf("/scb %s%n", s);
//                }
//            }
//        });
    }

    private void checkConfig() {
        var config = getConfig();
        // kijk of het de eerste keer is dan een example toevoegen;
        if (config.getKeys(false).isEmpty()) {
            config.set(CONFIG_PATH_IGNORE, List.of("Ignore1"));
            config.set(CONFIG_PATH_AMOUNT_OF_PLAYERS, 10);
            config.set(CONFIG_PATH_REFRESH_TIME, 100); // in ticks (20 ticks 1 seconden) dus 5 seconden
            saveConfig();
        } else {
            IGNORE_PLAYER_NAMES.addAll((ArrayList<String>) config.get(CONFIG_PATH_IGNORE));
            playerAmount = (Integer) config.get(CONFIG_PATH_AMOUNT_OF_PLAYERS);
            refreshTime = (Integer) config.get(CONFIG_PATH_REFRESH_TIME);
        }
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        if (args.length == 1) {
            return SCOREBOARD_INFO_MAP.keySet().stream()
                    .filter(s -> s.contains(args[0]))
                    .sorted(Comparator.naturalOrder())
                    .toList();
        }
        if (args.length == 2) {
            var scoreboardInfo = SCOREBOARD_INFO_MAP.get(args[0]);
            if (scoreboardInfo != null && scoreboardInfo.getSubEntitys() != null && !scoreboardInfo.getSubEntitys().isEmpty()) {
                return scoreboardInfo.getSubEntitys().keySet().stream()
                        .filter(s -> s.contains(args[1]))
                        .sorted(Comparator.naturalOrder())
                        .toList();
            }
        }
        return Collections.emptyList();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (label.equalsIgnoreCase(COMMAND_STRING)) {
            if (sender instanceof Player player) {
                if (args.length > 0) {
                    playerSubCommand.put(player, args);
                    stop(player);
                    start(player);
                }
            } else {
                sender.sendMessage(Component.text("Sorry only player stats"));
            }
            return true;
        }
        return false;
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent playerQuitEvent) {
        stop(playerQuitEvent.getPlayer());
    }

    private void addMine() {
        var mined = new HashMap<String, ScoreboardEntityTypeInfo>();
        Arrays.stream(Material.values())
                .filter(Material::isBlock)
                .forEach(material -> {
                    var name = material.getKey().getKey();
                    mined.put(name, new ScoreboardEntityTypeInfo(name, material));
                });
        mined.put("ore_total", new ScoreboardEntityTypeInfo("Total ores", true,
                Arrays.asList(COAL_ORE, DIAMOND_ORE, EMERALD_ORE, GOLD_ORE, IRON_ORE, LAPIS_ORE, REDSTONE_ORE, COPPER_ORE,
                        DEEPSLATE_COAL_ORE, DEEPSLATE_DIAMOND_ORE, DEEPSLATE_EMERALD_ORE, DEEPSLATE_GOLD_ORE, DEEPSLATE_IRON_ORE, DEEPSLATE_LAPIS_ORE, DEEPSLATE_REDSTONE_ORE, DEEPSLATE_COPPER_ORE,
                        NETHER_GOLD_ORE, NETHER_QUARTZ_ORE)));
        mined.put("log_total", new ScoreboardEntityTypeInfo("Total logs", true,
                Arrays.asList(OAK_LOG, DARK_OAK_LOG, BIRCH_LOG, ACACIA_LOG, JUNGLE_LOG, SPRUCE_LOG, CRIMSON_STEM, WARPED_STEM)));
        mined.put("planks_total", new ScoreboardEntityTypeInfo("Total planks", true,
                Arrays.asList(OAK_PLANKS, DARK_OAK_PLANKS, BIRCH_PLANKS, ACACIA_PLANKS, JUNGLE_PLANKS, SPRUCE_PLANKS, CRIMSON_PLANKS, WARPED_PLANKS)));
        mined.put("terracotta_total", new ScoreboardEntityTypeInfo("Total concrete", true,
                Arrays.asList(TERRACOTTA, WHITE_TERRACOTTA, ORANGE_TERRACOTTA, MAGENTA_TERRACOTTA, LIGHT_BLUE_TERRACOTTA, YELLOW_TERRACOTTA, LIME_TERRACOTTA, PINK_TERRACOTTA,
                        GRAY_TERRACOTTA, LIGHT_GRAY_TERRACOTTA, CYAN_TERRACOTTA, PURPLE_TERRACOTTA, BLUE_TERRACOTTA, BROWN_TERRACOTTA, GREEN_TERRACOTTA, RED_TERRACOTTA, BLACK_TERRACOTTA)));
        mined.put("concrete_total", new ScoreboardEntityTypeInfo("Total concrete", true,
                Arrays.asList(WHITE_CONCRETE, ORANGE_CONCRETE, MAGENTA_CONCRETE, LIGHT_BLUE_CONCRETE, YELLOW_CONCRETE, LIME_CONCRETE, PINK_CONCRETE, GRAY_CONCRETE, LIGHT_GRAY_CONCRETE, CYAN_CONCRETE
                        , PURPLE_CONCRETE, BLUE_CONCRETE, BROWN_CONCRETE, GREEN_CONCRETE, RED_CONCRETE, BLACK_CONCRETE)));
        mined.put("glass_total", new ScoreboardEntityTypeInfo("Total glass", true,
                Arrays.asList(GLASS, WHITE_STAINED_GLASS, ORANGE_STAINED_GLASS, MAGENTA_STAINED_GLASS, LIGHT_BLUE_STAINED_GLASS, YELLOW_STAINED_GLASS, LIME_STAINED_GLASS, PINK_STAINED_GLASS,
                        GRAY_STAINED_GLASS, LIGHT_GRAY_STAINED_GLASS, CYAN_STAINED_GLASS, PURPLE_STAINED_GLASS, BLUE_STAINED_GLASS, BROWN_STAINED_GLASS, GREEN_STAINED_GLASS, RED_STAINED_GLASS, BLACK_STAINED_GLASS, TINTED_GLASS)));
        SCOREBOARD_INFO_MAP.put("mined", new ScoreboardInfo("Mined ", MINE_BLOCK, mined));
    }

    private void addUse() {
        var used = new HashMap<String, ScoreboardEntityTypeInfo>();
        Arrays.stream(Material.values())
                .filter(Material::isItem)
                .forEach(material -> {
                    var name = material.getKey().getKey();
                    used.put(name, new ScoreboardEntityTypeInfo(name, material));
                });
        SCOREBOARD_INFO_MAP.put("used", new ScoreboardInfo("Used ", USE_ITEM, used));
    }

    private void addKilled() {
        var entitys = new HashMap<String, ScoreboardEntityTypeInfo>();
        Arrays.stream(EntityType.values())
                .filter(entityType -> entityType != UNKNOWN)
                .filter(entityType -> ClassUtils.getAllInterfaces(entityType.getEntityClass()).contains(Mob.class))
                .forEach(entityType -> {
                    var name = entityType.getKey().getKey();
                    entitys.put(name, new ScoreboardEntityTypeInfo(name, entityType));
                });
        SCOREBOARD_INFO_MAP.put("killed", new ScoreboardInfo("Killed ", KILL_ENTITY, entitys));
        SCOREBOARD_INFO_MAP.put("killed_by", new ScoreboardInfo("Killed_by ", ENTITY_KILLED_BY, entitys));
    }

    private void addStatistic() {
        SCOREBOARD_INFO_MAP.put(COMMAND_SUB_CLEAR, null);
        Arrays.stream(Statistic.values())
                .filter(statistic -> statistic.getType() == UNTYPED)
                .filter(statistic -> !statistic.getKey().getKey().contains("_one_cm")) // remove CM and add later own mapping
                .filter(statistic -> !statistic.getKey().getKey().contains("time")) // remove TIME and add later own mapping
                .filter(statistic -> !statistic.getKey().getKey().contains("play_one_minute")) // remove TIME and add later own mapping
                .forEach(statistic -> {
                    var name = statistic.getKey().getKey();
                    SCOREBOARD_INFO_MAP.put(name, new ScoreboardInfo(name, statistic));
                });
        fixTheDistanceAndTime();
        fixPlayIn();
    }

    private void fixPlayIn() {
        SCOREBOARD_INFO_MAP.put("played_in_ticks", new ScoreboardInfo("played_in_ticks", PLAY_ONE_MINUTE));
        SCOREBOARD_INFO_MAP.put("played_in_minutes", new ScoreboardInfo("played_in_minutes", PLAY_ONE_MINUTE, TICKS_TO_MINUTES));
        SCOREBOARD_INFO_MAP.put("played_in_hours", new ScoreboardInfo("played_in_hours", PLAY_ONE_MINUTE, TICKS_TO_HOURS));
    }

    private void fixTime(Statistic statistic) {
        var key = statistic.getKey().getKey();
        SCOREBOARD_INFO_MAP.put(key + "_in_ticks", new ScoreboardInfo(key + "_in_ticks", statistic));
        SCOREBOARD_INFO_MAP.put(key + "_in_minutes", new ScoreboardInfo(key + "_in_minutes", statistic, TICKS_TO_MINUTES));
        SCOREBOARD_INFO_MAP.put(key + "_in_hours", new ScoreboardInfo(key + "_in_hours", statistic, TICKS_TO_HOURS));
    }

    private void fixDistance(Statistic statistic) {
        var key = statistic.getKey().getKey().replace("_one_cm", ""); // TODO static strings van maken
        SCOREBOARD_INFO_MAP.put(key + "_in_cm", new ScoreboardInfo(key + "_in_cm", statistic));
        SCOREBOARD_INFO_MAP.put(key + "_in_meter", new ScoreboardInfo(key + "_in_meter", statistic, CM_TO_M));
        SCOREBOARD_INFO_MAP.put(key + "_in_km", new ScoreboardInfo(key + "_in_km", statistic, CM_TO_KM));
    }

    private void fixTheDistanceAndTime() {
        Arrays.stream(Statistic.values())
                .filter(statistic -> statistic.getType() == UNTYPED)
                .filter(statistic -> statistic.getKey().getKey().contains("time")) // fix everything with _TIME in it
                .forEach(this::fixTime);

        Arrays.stream(Statistic.values())
                .filter(statistic -> statistic.getType() == UNTYPED)
                .filter(statistic -> statistic.getKey().getKey().contains("_one_cm")) // fix everything with CM in it
                .forEach(this::fixDistance);
    }

    private void stop(Player player) {
        LobbyBoard lobbyBoard = new LobbyBoard(player.getUniqueId());
        if (lobbyBoard.hasID()) {
            lobbyBoard.stop();
        }
    }

    private void start(Player player) {
        taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            final LobbyBoard board = new LobbyBoard(player.getUniqueId());

            @Override
            public void run() {
                if (!board.hasID()) {
                    board.setID(taskId);
                }
                setScoreBoard(player, playerSubCommand.get(player));
            }
        }, 0, refreshTime);
    }

    private void setScoreBoard(Player player, String... name) {
        var scoreboardInfo = SCOREBOARD_INFO_MAP.get(name[0]);
        var scoreboardManager = Bukkit.getScoreboardManager();
        var playerScoreboard = scoreboardManager.getNewScoreboard();

        // if the scoreboardInfo is not found then do nothing
        if (scoreboardInfo != null) {
            ScoreboardEntityTypeInfo scoreboardEntityTypeInfo = null;
            String dislayname = scoreboardInfo.getDisplayname();
            if (name.length == 2) {
                scoreboardEntityTypeInfo = scoreboardInfo.getSubEntitys().get(name[1]);
                if (scoreboardEntityTypeInfo == null) {
                    player.setScoreboard(scoreboardManager.getMainScoreboard());
                    stop(player);
                    return;
                }
                dislayname += scoreboardEntityTypeInfo.getDisplayname();
            }
            var statistic = scoreboardInfo.getStatistic();
            if (statistic.getType().equals(UNTYPED) || name.length == 2) {
                final Objective objective = playerScoreboard.registerNewObjective("Scoreboard", Criteria.DUMMY,
                        Component.text("<< " + dislayname + " >>", NamedTextColor.GREEN).decorate(TextDecoration.BOLD));

                var scoreCount = new AtomicInteger(playerAmount + 3);
                var s1 = objective.getScore("Your score: " + ChatColor.YELLOW + calculateScore(getStatistic(player, statistic, scoreboardEntityTypeInfo), scoreboardInfo));
                var s2 = objective.getScore("");
                var s3 = objective.getScore("Top " + playerAmount + " players");
                s1.setScore(scoreCount.getAndDecrement());
                s2.setScore(scoreCount.getAndDecrement());
                s3.setScore(scoreCount.getAndDecrement());
                // get all the scores of the other players
                getPlayerScore(statistic, scoreboardEntityTypeInfo)
                        .forEach(stringIntegerEntry -> {
                            var sx = objective.getScore(stringIntegerEntry.getKey() + ": " + ChatColor.YELLOW + calculateScore(stringIntegerEntry.getValue(), scoreboardInfo));
                            sx.setScore(scoreCount.getAndDecrement());
                        });

                objective.setDisplaySlot(SIDEBAR);
                player.setScoreboard(playerScoreboard);
            } else {
                stop(player);
            }
        } else {
            if (name[0].equalsIgnoreCase("clear")) {
                player.setScoreboard(scoreboardManager.getMainScoreboard());
            }
            stop(player);
        }
    }

    private int getStatistic(OfflinePlayer player, Statistic statistic, ScoreboardEntityTypeInfo entityType) {
        if (entityType != null && entityType.getEntityType() != null) {
            return player.getStatistic(statistic, entityType.getEntityType());
        } else if (entityType != null && entityType.getMaterial() != null) {
            return player.getStatistic(statistic, entityType.getMaterial());
        } else if (entityType != null && entityType.isMerge()) {
            return entityType.getMaterials().stream()
                    .mapToInt(material -> player.getStatistic(statistic, material))
                    .sum();
        }
        return player.getStatistic(statistic);
    }

    private long calculateScore(long begin, ScoreboardInfo scoreboardInfo) {
        int calculateFactor = scoreboardInfo.getCalculateFactor();
        if (calculateFactor > 0) {
            return begin / scoreboardInfo.getCalculateFactor();
        }
        return begin;
    }

    private List<Map.Entry<String, Integer>> getPlayerScore(Statistic statistic, ScoreboardEntityTypeInfo entityType) {
        // TODO mee omgaan dat namen dubbel kunnen staan?
        return Arrays.stream(Bukkit.getOfflinePlayers())
                .collect(Collectors.toMap(
                        OfflinePlayer::getName,
                        player -> getStatistic(player, statistic, entityType)
                )).entrySet().stream()
                .filter(stringIntegerEntry -> !IGNORE_PLAYER_NAMES.contains(stringIntegerEntry.getKey()))
                .sorted(Map.Entry.comparingByValue(reverseOrder()))
                .limit(playerAmount)
                .collect(toList());
    }
}
